import Vue from 'vue'
import Vuetify from 'vuetify'
import DemoComponent from '@/components/DemoComponent'

Vue.use(Vuetify)

describe('DemoComponent.vue', () => {
  const Constructor = Vue.extend(DemoComponent)
  const vm = new Constructor().$mount()

  it('Should render correct contents', () => {
    expect(vm.$el.querySelector('h1.headline').textContent)
    .to.equal('Negotiation App')
  })

  it('Should validate maximum offer > 0 and not empty', done => {
    vm.employer.amount = 1

    var event = new Event('submit')
    vm.$el.querySelector('#employersForm').dispatchEvent(event)

    Vue.nextTick().then(() => {
      expect(vm.employer.formIsValid).to.equal(true)
    }).then(done, done)
  })

  it('Should validate minimum salary > 0 and not empty', done => {
    vm.employee.amount = 100

    var event = new Event('submit')
    vm.$el.querySelector('#employeesForm').dispatchEvent(event)

    Vue.nextTick().then(() => {
      expect(vm.employee.formIsValid).to.equal(true)
    }).then(done, done)
  })

  it('Should be SUCCESS when salary <= maximum offer', () => {
    vm.employee.amount = 10
    vm.employer.amount = 100

    expect(vm.isSuccess).to.equal(true)
  })
})
